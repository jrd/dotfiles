#!/bin/sh

# scrcpyd.sh - waits for USR1 signal to exec scrcpy

run()
{
    kill "$sleep_pid"
    exec scrcpy --no-audio --turn-screen-off --kill-adb-on-close
    adb kill-server
}

trap 'sleep 1; run' USR1
trap 'kill $sleep_pid' TERM

sleep infinity &
sleep_pid="$!"

echo "Waiting for USR1 signal on pid $$ ..."
wait "$sleep_pid"
