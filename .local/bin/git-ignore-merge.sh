#!/bin/sh

# Check if a filename is provided
if [ -z "$1" ]; then
    echo "Usage: $0 filename"
    exit 1
fi

FILENAME="$1"
GITATTRIBUTES=".gitattributes"
MERGE_STRATEGY="merge=ours"

if ! grep -q "$FILENAME $MERGE_STRATEGY" "$GITATTRIBUTES"; then
    echo "$FILENAME $MERGE_STRATEGY" >> "$GITATTRIBUTES"
    git config merge.ours.driver true
    git add "$GITATTRIBUTES"
    git commit -m "set merge strategy to use 'ours' for $FILENAME"
else
    echo "Config already set!"
fi



