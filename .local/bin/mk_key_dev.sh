#!/bin/sh

[ "$#" -ne 1 ] && printf "Error: Illegal number of arguments!\\n" && exit 1

DEVICE=$1

cryptsetup luksFormat "$DEVICE"
