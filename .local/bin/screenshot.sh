#!/bin/sh

# Takes a screenshot of a selected region.
# Author: jrd

case $1 in
      "") grim                                ;;
    area) grim -g "$(slurp)"                  ;;
    edit) grim -g "$(slurp)" - | swappy -f -  ;;
esac
