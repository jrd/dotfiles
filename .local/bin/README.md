
This is my collection of scripts.

### README.md




### READMEgenerator

This script generates the README file to the script collection.
It uses the descriptions that is hopefully present in the header of each
script.


### addShellScript

This script adds a new shell script template to the script directory and
opens it with nvim. After exiting nvim, adds and commits to the dotfile git
repository.
Author: jrd


### config

This shell script organizes the allocation and maintenance of personal
config files and shell scripts in a bare git repository called dotfiles.
Reference: https://www.atlassian.com/git/tutorials/dotfiles
Author: jrd


### print256colours.sh

Tom Hale, 2016. MIT Licence.
Print out 256 colours, with each number printed in its corresponding colour
See http://askubuntu.com/questions/821157/print-a-256-color-test-pattern-in-the-terminal/821163#821163


### slides2book

This script converts pdf presentation slides to an din a5 booklet which is
designed for printing.
Slightly adapted by 
Michael Roessler (http://www.pro-linux.de/kurztipps/2/1387/comm/454512/re-din-a5-buch-erstellen.html)
Dominik Mayer (dominik.mayer@gmail.com)


### vimv

Lists the current directory's files in Vim, so you can edit it and save to rename them
USAGE: vimv [file1 file2]
https://github.com/thameera/vimv


