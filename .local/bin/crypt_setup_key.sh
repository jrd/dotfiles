#!/bin/sh

# Ensure script is running as root, else elevate privileges
if [ "$(id -u)" != "0" ]; then
    if command -v sudo >/dev/null; then
        exec sudo "$0" "$@"
    elif command -v doas >/dev/null && [ -f /etc/doas.conf ]; then
        exec doas "$0" "$@"
    else
        su root -c "$0" "$@"
    fi
fi

DEVICE=$1

if [ $# -ne 1 ]
then
    printf "Error: Illegal number of arguments!\n"
    printf "Usage: crypt_setup_key.sh <device>\n"
    exit 1
fi

if [ ! -b "$DEVICE" ]
then
    echo "$DEVICE does not exist or is no block device!"
    exit 1
fi

if ! mountpoint -q /mnt/key; then
    echo "Source key device not found at /mnt/key/!"
    exit 1
fi

printf "WARNING!\n"
printf "========\n\n"
printf "Have you checked the device %s for integrity (badblocks)?\n" "$DEVICE"
printf "This will overwrite data on %s irrevocably.\n\n" "$DEVICE"
printf "========\n"
printf "Are you sure? (Type 'yes' in capital letters): "
read -r confirmation; [ ! "$confirmation" = YES ] && exit
wipefs "$DEVICE"

cryptsetup luksFormat --label KEY_CRYPT -yq "$DEVICE"

cryptsetup luksAddKey "$DEVICE" /etc/cryptsetup/key.key

cryptsetup open "$DEVICE" key-copy --key-file /etc/cryptsetup/key.key

mkfs.btrfs -L KEY -m dup -d dup /dev/mapper/key-copy

mkdir -pv /mnt/key-copy

mount -v /dev/mapper/key-copy /mnt/key-copy

rsync -ah /mnt/key/ /mnt/key-copy/

umount -v /mnt/key-copy

rmdir -v /mnt/key-copy

cryptsetup close /dev/mapper/key-copy
