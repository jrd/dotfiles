#!/bin/sh

input_file="$1"
temp_file=$(mktemp)

convert_date() {
    LC_TIME=de_DE.UTF-8 date -d "$1" "+%A, %d. %B %Y" | sed 's/^\(.\)/\U\1/'
}

# Zeilenweise die Datei durchgehen und das Datum umwandeln
while IFS= read -r line; do
    modified_line="$line"
    for date_str in $(echo "$line" | grep -oE "[0-9]{4}-[0-9]{2}-[0-9]{2}"); do
        converted_date=$(convert_date "$date_str")
        modified_line=$(echo "$modified_line" | sed "s/$date_str/$converted_date/")
    done
    echo "$modified_line" >> "$temp_file"
done < "$input_file"

mv "$temp_file" "new_$input_file"
