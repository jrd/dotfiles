#!/bin/sh

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <directory> <output_file>"
    exit 1
fi

DIRECTORY=$1
OUTPUT_FILE=$2

cd "$DIRECTORY" || exit

for FILE in $(ls -1 | sort -r); do
    if [ -f "$FILE" ]; then
        BASENAME=$(basename "$FILE")
        FILENAME="${BASENAME%.*}"
        echo "**$FILENAME**\n" >> "../$OUTPUT_FILE"
        cat "$FILE" >> "../$OUTPUT_FILE"
        echo "\n\n" >> "../$OUTPUT_FILE"
    fi
done

