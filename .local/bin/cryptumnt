#!/bin/sh

# crypt_mount - mount for crypt devices
#
# This script uses the information from /etc/crypttab to unmount and close
# devices with cryptsetup.

# Ensure script is running as root, else elevate privileges
if [ "$(id -u)" != "0" ]; then
    if command -v sudo >/dev/null; then
        exec sudo "$0"
    elif command -v doas >/dev/null && [ -f /etc/doas.conf ]; then
        exec doas "$0"
    else
        su root -c "$0"
    fi
fi

umount -av

# Read /etc/crypttab, ignoring commented lines, and process each entry
awk '!/^[[:space:]]*#/ {print $1}' /etc/crypttab |
while read -r name
do
    cryptsetup close "$name"
done
