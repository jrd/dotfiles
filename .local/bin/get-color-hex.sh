#!/bin/sh

# This script uses wayland tools (grim, slurp) and convert (ImageMagick) to
# print out hex color code of an given point on the screen.

hex_color_code=$(grim -g "$(slurp -p)" -t ppm - | convert -  txt:- | tail -n 1 | tr -s ' ' | cut -d ' ' -f 3 | cut -c2- | tr '[:upper:]' '[:lower:]')
wl-copy "$hex_color_code"
wl-copy -p "$hex_color_code"

echo "Copied hex code >$hex_color_code< to clipboard and promary selection!"
