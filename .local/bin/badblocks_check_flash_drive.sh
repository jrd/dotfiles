#!/bin/sh

# badblocks_check_flash_drive.sh - run badblocks with preset bad blocks limit
#
# EXIT STATUS
# 0: number of failed blocks less than bad blocks limit
# 1: number of failed blocks graeter than or equal to bad blocks limit

[ $# -ne 1 ] && printf "Error: Illegal number of arguments!\n" && exit 2

[ ! -b "$1" ] && printf "%s does not exist or is no block device!\n" "$1" && exit 2

# VARIABLES ###################################################################
DEVICE=$1
LOG=$(mktemp)
BLOCK_SIZE=$(blockdev --getbsz "$DEVICE")
# blockdev --getsz gets size in 512-byte sectors
TOTAL_BLOCKS=$(( $(blockdev --getsz /dev/sdb) * 512 / BLOCK_SIZE ))
ERROR_LIMIT="$(printf %.0f "$(echo "$TOTAL_BLOCKS * 0.0001" | bc)")"
###############################################################################

# Promt some device info and ask for validation:
fdisk -l "$DEVICE"
printf "\nBad blocks limit is set to 0.01%% "
printf "(%s of %s total blocks).\n" "$ERROR_LIMIT" "$TOTAL_BLOCKS"
printf "\nWARNING!\n========\n"
printf "This will take approx. 5 Minutes per GB.\n"
printf "This will wipe the device %s irreversibel!\n" "$DEVICE"
printf "========\nAre you sure? (Type 'yes' in capital letters):"
read -r confirmation; [ ! "$confirmation" = YES ] && exit

badblocks -ws -b "$BLOCK_SIZE" -o "$LOG" -e "$ERROR_LIMIT" "$DEVICE"

number_of_errors=$(wc -l "$LOG" | awk '{print $1}')

# Check if errors are within the limit
if [ "$number_of_errors" -lt "$ERROR_LIMIT" ]; then
    printf "Device passed with %d errors.\n" "$number_of_errors"
    exit 0
else
    printf "Device failed with %d errors, exceeding the limit of %d.\n" "$number_of_errors" "$ERROR_LIMIT"
    exit 1
fi
