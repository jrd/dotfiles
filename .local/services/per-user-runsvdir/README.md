# Per-User Services

> Sometimes it can be nice to have user-specific runit services.
> 
This is an implementation of the recommendation from the [official
documentation](https://docs.voidlinux.org/config/services/user-services.html)
to run services with user privileges and without the need of a user login.

## per-user-runsvdir
This service runs **runsvdir** with user privileges.

> [**runsvdir**(8)](https://man.voidlinux.org/runsvdir.8) - starts and monitors a collection of runsv processes  
> [**runsv**(8)](https://man.voidlinux.org/runsv.8) - starts and monitors a service and optionally an appendant log service

### Enable
To enable this service on a booted system, create a symlink to the service
directory in `/var/service/`:
```
ln -s /home/<user>/.config/per-user-runsvdir /var/service
```

### Add User Services
The user can then create new services or symlinks to them in the
`/home/<username>/.local/state/runsvdir/per-user` directory.

```
ln -s /home/<user>/.local/services/syncthing /home/<user>/.local/state/runsvdir/per-user
```
