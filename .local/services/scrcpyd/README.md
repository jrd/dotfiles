# scrcpyd

This runsv service starts [scrcpyd.sh](../../bin/scrcpyd.sh), a daemon that waits for a USR1 signal to
start [**scrcpy**](https://github.com/Genymobile/scrcpy). To pass the signal
one could write 1 to the named pipe scrcpyd/supervise/control (see
[**runsv**(8)#CONTROL](https://man.voidlinux.org/runsv.8#CONTROL)) or simply
use [**sv**](https://man.voidlinux.org/sv.8):
```
sv 1 /path/to/services/scrcpyd
```
