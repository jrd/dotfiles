#!/bin/sh

print_status() {
    status_line=$(bluetoothctl show | grep "Powered: ")
    powered=$(echo "$status_line" | cut -d " " -f2)
    printf "powered|string|%s\n\n" "$powered"
}

update_status() {
    print_status
    sleep 5 &
    sleep_pid="$!"
    wait "$sleep_pid"
}

trap 'update_status' USR1

# initial status update
printf "powered|string|%s\n\n" "unknown"

# Setteling time:
sleep 4

while true
do
    update_status
done
