#!/bin/sh
#
# power_button.sh - Trigger poweroff sequence by sending USR1 to this script,
#                   abort poweroff sequence by sending USR2 to thes script.
#
# USAGE: power_button.sh [countdown]
#
# TAGS:
#  Name        Type     Return
#  ---------------------------------------------
#  {status}    string   status of power button
#  {pid}       int      pid of this script
#  {countdown} int      time until poweroff is executed
#
# Exemples configuration:
#
#  - script:
#      path: /home/jrd/.config/yambar/bt-status.sh
#      args: [controller, F4:4E:E3:95:52:67]
#      content:
#        map:
#          default: {string: {text: , foreground: *color0 }}
#          conditions:
#            status == "on":
#              string: {text: , foreground: 01a0e4ff}
#
#  - script:
#      path: /home/jrd/.config/yambar/bt-status.sh
#      args: [device, EC:82:A0:C1:D3:63]
#      content:
#        map:
#          default: {string: {text: , foreground: *color0 }}
#          conditions:
#            status == "on":
#            - ramp:
#                tag: battery_percentage
#                items:
#                  - string: {text: , foreground: *color0}
#                  - string: {text: , foreground: *color1}
#                  - string: {text: , foreground: *color2}
#                  - string: {text: , foreground: *color3}
#                  - string: {text: , foreground: *color4}
#                  - string: {text: , foreground: *color5}
#                  - string: {text: , foreground: *color6}
#                  - string: {text: , foreground: *color7}
#                  - string: {text: , foreground: *color8}
#                  - string: {text: , foreground: *color9}

device_typ="$1"
device_mac="$2"

print_status(){
    if bluetoothctl "$option" "$device_mac" | grep --silent "$power_status"
    then
        echo "status|string|on"
        if [ "$battery_status" != "" ]
        then
            print_battery_status
        fi
        echo ""
    else
        echo "status|string|off"
        echo ""
    fi
}

print_battery_status() {
    status_line=$(bluetoothctl "$option" "$device_mac" | grep "$battery_status")
    battery_percentage=$(echo "$status_line" | cut -d "(" -f2 | cut -d ")" -f1)
    echo "battery_percentage|range:0-100|$battery_percentage"
}

update_status() {
    kill "$infinity_sleep"
    sleep "$settel_time"
    print_status
    sleep infinity &
    infinity_sleep="$!"
    wait "$infinity_sleep"
}

trap 'update_status' USR1

# Set bluetoothctl arguments:
case $device_typ in
    controller) option="show"
                power_status="Powered: yes"
                settel_time=4
                ;;
    device)     option="info"
                power_status="Connected: yes"
                battery_status="Battery Percentage"
                settel_time=2
                ;;
esac

# Print initial status:
echo "status|string|unknown"
echo ""

# initial status update
update_status
