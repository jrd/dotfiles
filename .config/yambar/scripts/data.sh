#!/bin/sh

# This POSIX script prints the status of an encrypted data disk in a yambar
# compatible format.

LABLE="DATA"
DEVICE="/dev/mapper/data"

# 1. Check if data partition is present:
if findfs LABLE="$LABLE"; then
    echo "present=true"
else
    echo "present=false"
fi

exit

# 2. Check if data partition is open and mounted
if cryptsetup status $DEVICE >/dev/null 2>&1; then
    echo "The device $DEVICE is open"

    if mount | grep --quiet $DEVICE; then
        echo "The device $DEVICE is mounted"
    else
        echo "The device $DEVICE is not mounted"
    fi
else
    echo "The device $DEVICE is not open or does not exist"
fi




output=$(btrfs filesystem usage "$1" 2> /dev/null)

device_size=$(echo "$output" | grep "Device size:" | awk '{ print $NF }' | sed 's/[^0-9.]*//g')
device_allocated=$(echo "$output" | grep "Device allocated:" | awk '{ print $NF }' | sed 's/[^0-9.]*//g')
percent_used=$(echo "scale=2; $device_allocated / $device_size * 100" | bc)
printf "percent_used|range:0-100|%.0f\n\n" "$percent_used"
