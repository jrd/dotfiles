#!/bin/sh

# countdown_cmd_button.sh - A yambar helper script.
#
# It helps to implement a yambar button that runs an arbitrary command on
# click, delayed by an arbitrary time. While one could implement this very
# easily with the label module combined with an on-click particle that runs
# 'sleep <time>; <cmd>', this script outputs the countdown as a tag and gives
# the chance to abort the countdown with another click. A typical use case
# would be a power-off button with a delay to correct misclicks.
# (Hint: Do not forget to set a nopassword exception for commands with root
# privileges)
#
# - script:
#     path: ~/.config/yambar/countdown_cmd_button.sh
#     args:
#       - doas /usr/bin/poweroff # command
#       - 10                     # countdown
#     content:
#       map:
#         conditions:
#           status == wait:
#             string:
#               text: 'Po'
#               on-click: kill -USR1 {pid}
#           status == countdown:
#             string:
#               text: "Poweroff in {countdown} seconds!"
#               on-click: kill -USR1 {pid}
#
# TAGS:
#  Name        Type     Return
#  ---------------------------------------------
#  {status}    string   status of script
#  {pid}       int      pid of this script
#  {countdown} int      time until command is executed


# GLOBAL VARIABLES
PID=$$
CMD=$1
DELAY=$2
#COUNTDOWN
#STATUS

update_tags()
{
    echo "status|string|$STATUS"
    echo "pid|int|$PID"
    echo "countdown|int|$COUNTDOWN"
    echo ""
}

countdown()
{
    while [ "$COUNTDOWN" -ge 0 ]; do
        update_tags
        COUNTDOWN=$((COUNTDOWN - 1))
        sleep 1 &
        wait $!
    done
}

sleep_infinitely()
{
    sleep infinity &
    sleep_pid="$!"
    wait "$sleep_pid"
}

kill_sleep()
{
    kill $sleep_pid
}

do_wait()
{
    COUNTDOWN=$DELAY
    STATUS="wait"
    update_tags
    sleep_infinitely
}

do_countdown()
{
    kill_sleep
    STATUS="countdown"
    countdown
    sh -c "$CMD"
    do_wait
}

on_click()
{
    case $STATUS in
        wait) do_countdown ;;
        countdown) do_wait ;;
    esac
}

trap 'on_click' USR1
trap 'kill_sleep' TERM

# initial status:
do_wait
