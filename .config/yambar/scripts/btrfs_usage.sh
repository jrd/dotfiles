#!/bin/sh

MOUNTPOINT=$1

DATA_LINE=$(btrfs filesystem df -b "$MOUNTPOINT" | grep '^Data,')
DATA_USED=$(echo "$DATA_LINE" | grep -Po 'used=\K[^\s,]+')

DEVICE=$(df -h "$MOUNTPOINT" | tail -1 | awk '{print $1}')
DISK_SIZE=$(lsblk -o SIZE -b "$DEVICE" | tail -n 1)

DATA_PERCENTAGE=$(echo "scale=2; $DATA_USED / $DISK_SIZE * 100" | bc)

printf "percent_used|range:0-100|%.0f\n\n" "$DATA_PERCENTAGE"

