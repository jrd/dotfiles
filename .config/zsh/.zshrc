# Config for zsh

# History in cache directory ##################################################
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.config/zsh/.history
###############################################################################

# Basic auto/tab complete #####################################################
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
_comp_options+=(globdots)		# Include hidden files.
compinit
###############################################################################

# yazi integration ############################################################
if whence yazi > /dev/null; then
    ya()
    {
	    local tmp="$(mktemp -t "yazi-cwd.XXXXX")"
	    yazi "$@" --cwd-file="$tmp"
	    if cwd="$(cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
	    	cd -- "$cwd"
	    fi
	    rm -f -- "$tmp"
    }
fi
###############################################################################

# eza integration #############################################################
if whence eza > /dev/null
then
    alias ls='eza --icons --git --group-directories-first --time-style=long-iso'
    alias lz='eza --icons --git --group-directories-first --time-style=long-iso'
    alias l='ls -lbF'
    alias ll='ls -la'
    alias llm='ll --sort=modified'
    alias la='ls -lbhHigUmuSa'
    alias lx='ls -lbhHigUmuSa@'
    alias tree='eza --tree'
else
    echo "eza not accessible!"
fi
###############################################################################

# ALIASES #####################################################################
alias wget='wget --no-hsts'
alias bt='bluetoothctl list && bluetoothctl devices && bluetoothctl'
###############################################################################

# KEYBINDINGS #################################################################
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down
###############################################################################

# PLUGINS #####################################################################
if [[ ! -f $ZDOTDIR/.zcomet/bin/zcomet.zsh ]]; then
    command git clone https://github.com/agkozak/zcomet.git $ZDOTDIR/.zcomet/bin
fi
source $ZDOTDIR/.zcomet/bin/zcomet.zsh
zcomet load spaceship-prompt/spaceship-prompt spaceship.zsh
zcomet load ohmyzsh plugins/dirhistory
zcomet load zsh-users/zsh-autosuggestions
zcomet load zsh-users/zsh-history-substring-search
zcomet load zsh-users/zsh-completions
zcomet load zsh-users/zsh-syntax-highlighting
zcomet compinit
###############################################################################
