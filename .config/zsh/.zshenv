# User Environment Variables

export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/.local/share/nvim/mason/bin:$PATH"

# https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"

# default programs
export BROWSER="firefox"
export PAGER="less"
export EDITOR="nvim"
export SUDO_EDITOR="nvim"
export TERMINAL="foot"
export VISUAL="nvim"
export BROWSER="firefox"
export READER="zathura"
export OPENER="xdg-open"

# wayland
export QT_QPA_PLATFORM="wayland"
export MOZ_ENABLE_WAYLAND=1
export ANKI_WAYLAND=1
export ELECTRON_OZONE_PLATFORM_HINT="wayland"

# clean home dir
export LESSHISTFILE="/dev/null"
export MBSYNCRC="$XDG_CONFIG_HOME/mbsync/mbsyncrc"
export GNUPGHOME="/mnt/key/gnupg"

# Colored man
export MANPAGER="less --quit-if-one-screen --RAW-CONTROL-CHARS --use-color -Dd+g -Du+b"

# pass
export PASSWORD_STORE_DIR="/home/jrd/Data/.password-store"
export PASSWORD_STORE_X_SELECTION="wl-copy"
export PASSWORD_STORE_ENABLE_EXTENSIONS="true"

export SVDIR="$XDG_STATE_HOME/runsvdir/dwl/"
