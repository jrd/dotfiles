if [[ -z $DISPLAY && $TTY = /dev/tty1 ]]
then
    # pipewire wants a session bus, so
    dbus-run-session dwl -s 'runsvdir -P /home/jrd/.local/state/runsvdir/dwl <&-'
fi
