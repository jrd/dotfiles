**Disclaimer: This document deals with my personal configurations and serves me as an overview and reminder. For outsiders no distinct enlightenment can be granted.**

# Configurations

1. [Introduction](#introduction)
2. [Guiding Principles](#guiding-principles)
    1. [Avoid Configuration Files](#avoid-configuration-files)
    2. [Clean HOME Directory](#clean-home-directory)
3. [Hardware](#hardware)
    1. [BRIX](#brix) 
    2. [Z790](#z790)
    3. [T470](#t470)
    4. [Pixel3a](#pixel3a)
5. [Operating Systems](#operating-systems)
    1. [Windows](#windows)
    2. [Ubuntu](#ubuntu)
    3. [Void Linux](#void-linux)
    4. [GrapheneOS](#grapheneos)
6. [Programs](#programs)

## Introduction

These are my configurations for various programs on my computer. The computer is my daily tool. A tool to create. A tool to tap sources of information. A tool of communication. A tool of entertainment and diversion. A tool that is in my hand every day. It must fit to it and must be adapted to the highest degree to my whole person, must become part of life. On which properties of my tool do I have influence? Lots of configurations can be influenced. The question should be: on which configurations do I influence in a meaningful way? What does meaningful mean?

I am just a simple man, I prefer simplicity and clarity. The configuration of each program is a desperate attempt to cope with life itself. Each config file should stand for itself. Nevertheless, I want to record in this document what drove me to certain configurations, and, to forestall my creeping forgetfulness, how to make other adjustments to my system that might not be easily immortalized in a config file.


## Guiding Principles

### I. Avoid Configuration Files

Avoiding configuration files where possible is an important ingredient to a happy life. It saves the effort of maintenance, synchronization and porting. It also avoids conflicts after serious program updates. Sometimes an outdated configuration file prevents the full functionality of a software, because the developers have meanwhile considered a feature as stable and set it as default, while it is still excluded in the existing configuration. But of course, we should also not be devoted to the supposedly well-meaning tastes of the authorities. My German grandparents once made this mistake with disastrous consequences. At the appropriate point, no or false must be said, the switch must be flipped to a better alternative, the flag must be set and one's colors must be nailed to the mast.


### II. Clean HOME Directory

I have a deep aversion to a home directory cluttered with countless dotfiles. I don't know why, nor do I want to waste expensive therapy time trying to figure it out. It is simply my innermost desire to have a clean user home directory.

There are **four main ways** to prevent a program from polluting the home directory:

1. Developers are wise and insightful and comply with the [**XDG Base Directory Specification**](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html). Then only the corresponding environment variables `$XDG_DATA_HOME` and `$XDG_CONFIG_HOME` must be adapted.

2. The program asks for **program specific environment variables** to look for configuration and data files.

3. The program accepts **command line arguments** (e.g. `--config=path_to_config_file`). This leads to often ugly solutions like defining shell aliases or starter scripts.

4. The trivial solution is to **not using the program** at all.

Places to stop by when looking for solutions to specific programs:

* [reddit](https://www.reddit.com/r/linux/comments/971m0z/im_tired_of_folders_littering_my_home_directory/)
* [archwiki](https://wiki.archlinux.org/title/XDG_Base_Directory)


## Hardware

### BRIX

GIGABYTE Ultra Compact PC Kit GB-BXCE-3205

**System Info:**
* CPU: Intel Celeron 3205U (2) @ 1.500GHz
* GPU: Intel HD Graphics
* MEM: 8GB
* Network: Gigabit Lan
* OS: [Void Linux](#void-linux)


### Z790

PC based on the Z790 chipset build in February 2023.

**System Info:**
* CPU: 13th Gen Intel i5-13600K (20) @ 3.800GHz
* GPU: Intel Arc A770 (16G)
* MEM: 64GB
* Network: Intel Wi-Fi 6E AX210, 2.5 Gb LAN
* OS: [Windows](#windows), [Ubuntu](#ubuntu), [Void Linux](#void-linux) (Triple Boot)


### T470

Laptop ThinkPad T470 purchased (second hand) in November 2020.

**System Info:**
* CPU: Intel Core i5-6300U @ 4x 3GHz
* GPU: HD Graphics 520 (internal)
* MEM: 16GB
* Network: Intel Wi-Fi 6E AX211, 1 Gb LAN
* OS: [Windows](#windows), [Ubuntu](#ubuntu), [Void Linux](#void-linux) (Triple Boot)


### Pixel3a

Mobil phone Google Pixel 3a purchased (second hand) in Oktober 2020.

**System Info:**
* Chipset: Qualcomm SDM670 Snapdragon 670 (10 nm)
* CPU: Octa-core (2x2.0 GHz 360 Gold & 6x1.7 GHz Kryo 360 Silver)
* GPU: Adreno 615
* MEM: 4GB
* OS: [GrapheneOS](#grapheneos)


## Operating Systems

Call it fate, call it obsessive-compulsive disorder, call it regaining a foothold in an oh-so uncertain and aimless world - I find myself having to reinstall an operating system more often than not. In this section OS specific configurations shall be noted.


### Windows

There are a few [Reasons not to use Microsoft](https://stallman.org/microsoft.html)'s products and especially [Microsoft's Software](https://www.gnu.org/proprietary/malware-microsoft.html).

But I am, I cannot stress this enough, only a simple man. I do what simple men do: Compromise values and beliefs in order to adapt and get by. Compromise values and beliefs in order to adapt and get by? There's a strange feeling in my gut, maybe I've eaten too much German sausage.

Recently, Microsoft invested a few billion of its monopoly money in a chatbot. Let's use its words:

*Tell me the best reason not to use MS Windows and the best reason to use it.*

>The best reason not to use MS Windows is concerns about privacy and data security. There have been instances in the past where Windows has been vulnerable to viruses, malware, and other security threats, which can compromise users' privacy and data. Additionally, there have been concerns about Windows' data collection practices, which may include gathering information about users' activity, hardware, and software usage.

>The best reason to use MS Windows is its compatibility with a wide range of software and applications. As the dominant operating system on personal computers, many software developers prioritize developing for Windows, making it easier to find and use software that meets your needs.

Accessible and easy to find like highway restrooms. Just don't touch more than necessary!
Here are some step for a fresh Windows installation:

1. **Disable Hibernation**   
    Run in Command Promt as administrator:
    ```
    powercfg.exe /hibernate off
    ```

2. **Set Time to UTC**   
Run in Command Promt as administrator:
    ```
    Reg add HKLM\SYSTEM\CurrentControlSet\Control\TimeZoneInformation /v RealTimeIsUniversal /t REG_QWORD /d 1
    ```

3. **Install Media Feature Pack for Windows**   
    * On Windows 10 N: Settings > Apps > Apps and Features > Optional features > Add a feature.
    * On Windows 11 N: Settings > Apps > Optional features.

4. **Install Windows Subsystem for Linux (WSL)**   
    Run in Command Promt as administrator:
    ```
    wsl --install
    ```


### Ubuntu

Ubuntu gets the job done. It's the go-to when some program demands a mainstream OS before you're forced to fire up that dusty old Windows machine.
But my home, it seems, it never truly was. Even if it was my first fleeting glance into the world of Linux computing. All this GUI shenanigans and backgroud actions (to use the technical terms) leave my laptop fan restless and my simple mind without any hope of clarity.
Therefore, not too much should be touched here as well. After all, we don't want to ruin what still attracts us today: the well-defined environment for the fine gentlemen of mainstream software.

Some considerations must nevertheless be made:

1. **Neovim**

    Parts of my neovim configs require a newer neovim version, witch is available [here](https://github.com/neovim/neovim/wiki/Installing-Neovim#ubuntu). Further the plugin *mason.nvim* requires a newer version of nodejs (to install language servers), which is available [here](https://github.com/nodesource/distributions#installation-instructions). Executing the command `nvim -c checkhealth` will not cause any harm and may help identify any missing components.

2. **Nerd Fonts**

    Considering the lack of Nerd Fonts within the Ubuntu packages, a manual installation is also required here. Follow this [link](https://github.com/ryanoasis/nerd-fonts#option-3-install-script).

3. **Configuration**
    
    Use these dotfiles without any worries. Otherwise, leave everything as it is to keep the system compatible with the most common instructions for applications that expect an Ubuntu operating system. Use fish for a little more advanced command line action, zsh might clash with Ubuntu due to the environment variables included in these dotfiles and tuned to void linux and wayland.


### Void Linux


### GrapheneOS


## Programs

* [runit](runit/README.md)


