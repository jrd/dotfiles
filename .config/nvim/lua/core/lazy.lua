-----------------------------------------------------------
-- Plugin manager configuration file
-----------------------------------------------------------

-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- Use a protected call so we don't error out on first use
local status_ok, lazy = pcall(require, 'lazy')
if not status_ok then
  return
end

-- Start setup ----------------------------------------------------------------
lazy.setup({ spec = {
-------------------------------------------------------------------------------

-- alpha-nvim - a fast and fully programmable greeter for neovim  ----------{{{
{
  'goolord/alpha-nvim',
  dependencies = { 'nvim-tree/nvim-web-devicons' },
},
----------------------------------------------------------------------------}}}

-- gitsigns - Super fast git decorations implemented purely in Lua  --------{{{
{
  'lewis6991/gitsigns.nvim',
  lazy = true,
  dependencies = {
    'nvim-lua/plenary.nvim',
    'nvim-tree/nvim-web-devicons',
  },
  config = function()
    require('gitsigns').setup{}
  end
},
----------------------------------------------------------------------------}}}

-- nvim-tee - A file explorer tree for neovim written in lua ---------------{{{
{
  'nvim-tree/nvim-tree.lua',
  dependencies = { 'nvim-tree/nvim-web-devicons' },
},
----------------------------------------------------------------------------}}}

-- nvim-treesitter - Nvim Treesitter configurations and abstraction layer --{{{
{
  'nvim-treesitter/nvim-treesitter',
  build = ':TSUpdate'
},
----------------------------------------------------------------------------}}}

-- indent-blankline - Indent guides for Neovim -----------------------------{{{
{ "lukas-reineke/indent-blankline.nvim", main = "ibl", opts = {} };
----------------------------------------------------------------------------}}}

-- nvim-autopairs - insert or delete brackets, parens, quotes in pair ------{{{
{
  'windwp/nvim-autopairs',
  event = 'InsertEnter',
  config = function()
    require('nvim-autopairs').setup{}
  end
},
----------------------------------------------------------------------------}}}

-- mason - package manager for Neovim --------------------------------------{{{
{
  "williamboman/mason.nvim",
  lazy = false,
  config = function()
    require("mason").setup{}
  end,
  build = ":MasonUpdate"
},
----------------------------------------------------------------------------}}}

-- mason-lspconfig - Extension to use lspconfig with mason -----------------{{{
{
  "williamboman/mason-lspconfig.nvim",
  lazy = false,
  config = function()
    require("mason-lspconfig").setup{}
  end
},
----------------------------------------------------------------------------}}}

-- nvim-lspconfig - Quickstart configs for Nvim LSP ------------------------{{{
{
  "neovim/nvim-lspconfig",
  lazy=false
},
----------------------------------------------------------------------------}}}

-- nvim-cmp - A completion plugin for neovim coded in Lua ------------------{{{
{
  'hrsh7th/nvim-cmp',
  event = 'InsertEnter',
  dependencies = {
    'L3MON4D3/LuaSnip',
    'hrsh7th/cmp-nvim-lsp',
    'hrsh7th/cmp-path',
    'hrsh7th/cmp-buffer',
    'saadparwaiz1/cmp_luasnip',
  },
},
----------------------------------------------------------------------------}}}

-- markdown-preview --------------------------------------------------------{{{
{
  "iamcco/markdown-preview.nvim",
  ft = "markdown",
  config = function()
    vim.fn["mkdp#util#install"]()
    vim.g.mkdp_browser = '/usr/bin/firefox'
  end,
},
----------------------------------------------------------------------------}}}

-- vimwiki - a personal wiki for Vim ---------------------------------------{{{
{
  'vimwiki/vimwiki',
  lazy = false,
  init = function()
    vim.g.vimwiki_list = {
      {
        path = '~/repos/wiki',
        syntax = 'markdown',
        ext = '.md',
        path_html = '~/repos/wiki/html/',
        custom_wiki2html = 'vimwiki_markdown'
      }
    }
    vim.g.vimwiki_ext2syntax = {
      ['.md'] = 'markdown',
      ['.markdown'] = 'markdown',
      ['.mdown'] = 'markdown'
    }
    vim.g.vimwiki_use_mouse = 1
    vim.g.vimwiki_markdown_link_ext = 1
  end,
},
----------------------------------------------------------------------------}}}

-- true-zen.nvim - Clean and elegant distraction-free writing for NeoVim ---{{{
{
  'Pocco81/true-zen.nvim',
  config = function()
    require("true-zen").setup {}
  end
},
----------------------------------------------------------------------------}}}

-- lualine - A blazing fast and easy to configure Neovim statusline --------{{{
{
  'nvim-lualine/lualine.nvim',
  dependencies = { 'nvim-tree/nvim-web-devicons' },
  config = function()
    require("lualine").setup{}
  end
},
----------------------------------------------------------------------------}}}

{
  "folke/trouble.nvim",
  keys = {
    {
      "<leader>xx",
      "<cmd>Trouble diagnostics toggle<cr>",
      desc = "Diagnostics (Trouble)",
    },
    {
      "<leader>xX",
      "<cmd>Trouble diagnostics toggle filter.buf=0<cr>",
      desc = "Buffer Diagnostics (Trouble)",
    },
    {
      "<leader>cs",
      "<cmd>Trouble symbols toggle focus=false<cr>",
      desc = "Symbols (Trouble)",
    },
    {
      "<leader>cl",
      "<cmd>Trouble lsp toggle focus=false win.position=right<cr>",
      desc = "LSP Definitions / references / ... (Trouble)",
    },
    {
      "<leader>xL",
      "<cmd>Trouble loclist toggle<cr>",
      desc = "Location List (Trouble)",
    },
    {
      "<leader>xQ",
      "<cmd>Trouble qflist toggle<cr>",
      desc = "Quickfix List (Trouble)",
    },
  },
  opts = {}, -- for default options, refer to the configuration section for custom setup.
},

{
  'hat0uma/csvview.nvim',
  config = function()
    require('csvview').setup()
  end
}

},})
